﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.IO;
using Topshelf.Runtime.Windows;
using System.Diagnostics;
using System.Media;
using System.Collections;
namespace AltosGo
{
    public partial class AltosGo : Form
    {
        SpeechRecognitionEngine Prepoznaj = new SpeechRecognitionEngine();
        SpeechSynthesizer Altos = new SpeechSynthesizer();
        SpeechRecognitionEngine Slusaj = new SpeechRecognitionEngine();
        Stack<Process> proc = new Stack<Process>();
        int broj = 0;
        SoundPlayer media;
        Random VremeRazgovora = new Random();
         int vremeR = 0;
        // DateTime vreme = new DateTime();
        public AltosGo()
        {
            InitializeComponent();
        }

        private void AltosGo_Load(object sender, EventArgs e)
        {
            Prepoznaj.SetInputToDefaultAudioDevice();
            Prepoznaj.LoadGrammarAsync(new Grammar(new GrammarBuilder(new Choices(File.ReadAllLines(@"Komande.txt")))));
            Prepoznaj.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(Default_SpeechRecognized);
            Prepoznaj.SpeechDetected += new EventHandler<SpeechDetectedEventArgs>(Prepoznaj_SpeechRecognized);
            Prepoznaj.RecognizeAsync(RecognizeMode.Multiple);


            Slusaj.SetInputToDefaultAudioDevice();
            Slusaj.LoadGrammarAsync(new Grammar(new GrammarBuilder(new Choices(File.ReadAllLines(@"Komande.txt")))));
            Slusaj.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(Slusaj_SpeechRecognized);

            Altos.SpeakAsync("Welcome to AltosGo");
        }

        private void StopericaRazgovora_Tick(object sender, EventArgs e)
        {
            if (vremeR == 10)
            {
                Prepoznaj.RecognizeAsyncCancel();
            }
            else 
                if (vremeR == 11)
            {
                StopericaRazgovora.Stop();
                Slusaj.RecognizeAsync(RecognizeMode.Multiple);
                //ako se prepozna neki govor pokrece tajmer
                vremeR = 0;
            }
            vremeR = (StopericaRazgovora.Interval % 1000) + 1; //da broji
        }

        private void Default_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            int ranNum;
            string govor = e.Result.Text; //sta je prepoznao od mic 

            //string[] komande = (File.ReadAllLines(@"Komande.txt")); 

            //if (komande.AsQueryable().Contains<string>(govor)) //znaci da postoji u listi komande 
            //{
            //    foreach (string com in komande)
            //    {
            //        if (com == govor)
            //        {

            //        }
            //    }
            //}
            //else

            //{
            //    Altos.SpeakAsync("I am sorry. I don t understend command");
            //}

            if (govor.Contains("Mip mip"))
            {
                Altos.SpeakAsync("Oo hellou dear ");

            }
            if (govor.Contains("Hello"))
            {
                Altos.SpeakAsync("Hello, I am here for you");

            }
            if (govor.Contains("I am"))
            {
                Altos.SpeakAsync("You are my boss");
               

            }
            if (govor.Contains("Time"))
            {
                Altos.SpeakAsync("Time is " + DateTime.Now.ToString("t"));
             
            }
            if (govor.Contains("Date"))
            {
                Altos.SpeakAsync("Date is " + DateTime.Now.ToString("D"));

            }
            if (govor.Contains("Stop"))
            {
                Altos.SpeakAsyncCancelAll();
                ranNum = VremeRazgovora.Next(1, 2);
                if (ranNum == 1)
                {
                    Altos.SpeakAsync("Yes sir");
                    Prepoznaj.RecognizeAsyncCancel();
                }

                if (ranNum == 2)
                {
                    Altos.SpeakAsync("I am sorry i will be quiet");
                    Prepoznaj.RecognizeAsyncCancel();
                }

            }
            if (govor.Contains("Break"))
            {
                Altos.SpeakAsync("If you need me just say");
                Prepoznaj.RecognizeAsyncCancel();
                Slusaj.RecognizeAsync(RecognizeMode.Multiple);

            }
            if (govor.Contains("Show commands"))
            {
                string[] commands = File.ReadAllLines(@"Komande.txt");
                LbKomande.Items.Clear();
                LbKomande.SelectionMode = SelectionMode.None;
                LbKomande.Visible = true;


                foreach (string s in commands)
                {
                    LbKomande.Items.Add(s);
                }

            }
            if (govor.Contains("Hide commands"))
            {
                LbKomande.Visible = false;

            }
            if (govor.Contains("Goodbay"))
            {
               //Altos.SpeakAsync("See you soon. ");
               // Prepoznaj.RecognizeAsyncCancel();
               // Slusaj.RecognizeAsync(RecognizeMode.Multiple);
               
             
                this.Close();
            }
            if (govor.Contains("How are you"))
            {
               
                Altos.SpeakAsync("I am fine, how are you");
                Prepoznaj.RecognizeAsyncCancel();
                Slusaj.RecognizeAsync(RecognizeMode.Multiple);
            }
            if (govor.Contains("Open paint"))
            {
              
                Altos.SpeakAsync("Wait a moment.");
                // Process p = Start("mspaint.exe");
                Process p = new Process(); 
                p = Process.Start("mspaint.exe"); //pamti sve za dati proces 
                proc.Push(p);
                broj++;
                Prepoznaj.RecognizeAsyncCancel();
                Slusaj.RecognizeAsync(RecognizeMode.Multiple);


            }
            if (govor.Contains("Open calculator"))
            {

                Altos.SpeakAsync("Wait a moment.");
                Process p = new Process();
                 p = Process.Start("calc.exe");
                broj++;
                proc.Push(p);
                Prepoznaj.RecognizeAsyncCancel();
                Slusaj.RecognizeAsync(RecognizeMode.Multiple);


            }
            if (govor.Contains("Music stop"))
            {
               
                Altos.SpeakAsync("Ok sir");
                pBAnimacija.Visible = false;
                media.Stop();
              
            }


        }

     

        private void Prepoznaj_SpeechRecognized(object sender, SpeechDetectedEventArgs e)
        {
            vremeR = 0;

        }
        private void Slusaj_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            string govor = e.Result.Text;


            if (govor.Contains("Wake up"))
            {
                Slusaj.RecognizeAsyncCancel();
                Altos.SpeakAsync("HI");
                Prepoznaj.RecognizeAsync(RecognizeMode.Multiple);

            }
            if (govor.Contains("Altos"))
            {
                Slusaj.RecognizeAsyncCancel();
                Altos.SpeakAsync("I i i i am  Altos 5");
                Prepoznaj.RecognizeAsync(RecognizeMode.Multiple);

            }

            if (govor.Contains("Who you are"))
            {
                Slusaj.RecognizeAsyncCancel();
                Altos.SpeakAsync("I am Altos and i am hire for you sir. Give me command");
                Prepoznaj.RecognizeAsync(RecognizeMode.Multiple);

            }
            if (govor.Contains("Close paint"))
            {
                Slusaj.RecognizeAsyncCancel();
                Altos.SpeakAsync("Just a second");

                // proc.ElementAt(--broj).Kill();
                Process p = proc.Pop();
             
                if (p != null)
                {
                 
                    try
                    {
                        foreach (var process in Process.GetProcessesByName("mspaint"))
                        {
                            process.Kill();
                        }
                    }
                    catch (Exception le)
                    {
                        Altos.SpeakAsync("Exception is trown!");
                    }
                }
                else
                {
                    Altos.SpeakAsync("Aplication is not open!");
                }
                //p.Close();
                //p.Kill();
                Prepoznaj.RecognizeAsync(RecognizeMode.Multiple);

            }
            if (govor.Contains("Close calculator"))
            {
                Slusaj.RecognizeAsyncCancel();
                Altos.SpeakAsync("Just a minute");

                //  proc.ElementAt(--broj).Kill();
                Process p = proc.Pop();
                if (p != null)
                {
                  
                    try
                    {
                        foreach (var process in Process.GetProcessesByName("calc"))
                        {
                            process.Kill();
                        }
                    }
                    catch (Exception le)
                    {
                        Altos.SpeakAsync("Exception is trown!");
                    }
                }
                else
                {
                    Altos.SpeakAsync("Aplication is not open!");
                }
                Prepoznaj.RecognizeAsync(RecognizeMode.Multiple);

            }
            if (govor.Contains("Fine") || govor.Contains("Good"))
            {
                Slusaj.RecognizeAsyncCancel();
                Altos.SpeakAsync("That is good sir");
                Prepoznaj.RecognizeAsync(RecognizeMode.Multiple);
            }
            if (govor.Contains("Bad") || govor.Contains("Bored"))
            {
                Slusaj.RecognizeAsyncCancel();
                Altos.SpeakAsync("I play music for you");
                pBAnimacija.SizeMode = PictureBoxSizeMode.StretchImage;
              // var MyImage = new Bitmap(Properties.Resources.mk.);
                pBAnimacija.ClientSize = new Size(197, 86);
                pBAnimacija.Image = (Image)Properties.Resources.mk;
                pBAnimacija.Visible = true;
               // media = new System.Media.SoundPlayer();
                //media.SoundLocation = @"D:\faks\C#\AltosGo\AltosGo\flasa.wav";
                //  media.SoundLocation = Properties.Resources.flasa.
               media = new SoundPlayer(Properties.Resources.flasa);
              //  media.Play();

                media.PlayLooping();
                Prepoznaj.RecognizeAsync(RecognizeMode.Multiple);
            }
          

        }


      
    }
}
